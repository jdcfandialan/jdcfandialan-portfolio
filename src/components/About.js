import React from 'react';
import {Parallax} from 'react-parallax';

export default function About(){
    const insideStyles = {
        width: '100%',
        background: "white",
        padding: 20,
        paddingLeft: '20%',
        paddingRight: '20%',
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)"
    }

    return (
        <Parallax bgImage={require("../images/bg_parallax.jpg")} strength={500}>
            <div style={{height: '80vh'}}>
                <div style={insideStyles} className="d-flex flex-column align-items-center justify-content-center">
                    <h3 className="py-5">Hello, you can call me <span style={{fontWeight: 'bold', color: '#e94f4f'}}>Didi</span>.</h3>
                    <p className="pb-5">I studied BS Computer Science at the University of the Philippines, Los Baños and completed Zuitt's intensive Coding Bootcamp. I am a full-stack web developer currently working as a React front-end developer.</p>
                </div>
            </div>
        </Parallax>
    )
}