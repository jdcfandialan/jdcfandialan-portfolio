import React from 'react';
import {Parallax} from 'react-parallax';
import Project from './Project';

export default function Divider(){
    const insideStyles = {
        width: '100%',
        background: "white",
        padding: 20,
        paddingLeft: '20%',
        paddingRight: '20%',
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)"
    }
    
    return (
        <>
            <div style={{backgroundColor: 'black', height: '20vh'}}></div>

            <Parallax bgImage={require("../images/bg_project.jpg")} strength={500}>
                <div style={{height: '70vh'}}>
                    <div style={insideStyles} className="d-flex flex-column align-items-center justify-content-center">
                        <h3 className="py-5" style={{color: '#e94f4f'}}>Projects</h3>
                    </div>
                </div>
            </Parallax>

            <Project />

        </>
    )
}