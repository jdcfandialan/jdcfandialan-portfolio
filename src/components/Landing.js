import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {LandingImg} from '../images/index';

export default function Landing(){
    const LandingStyle = {
        backgroundColor: 'black'
    }

    const imgStyle = {
        objectFit: 'contain',
        height: '85%',
        width: '80vw'
    }

    return (
        <div className="vh-100 d-flex flex-column justify-content-center align-items-center" style={LandingStyle}>
            <img src={LandingImg} alt="" style={imgStyle} className="pb-1"/>
            
            <div className="d-flex justify-content-center align-items-center pb-5">
                <a className="text-secondary" href="https://www.facebook.com/jdcfandialan/" target="_blank" rel="noreferrer noopener">
                    <FontAwesomeIcon icon={['fab', 'facebook']} className="mx-4 fa-2x" />
                </a>

                <a className="text-secondary" href="https://twitter.com/dominicology" target="_blank" rel="noreferrer noopener">
                    <FontAwesomeIcon icon={['fab', 'twitter']} className="mx-4 fa-2x" />
                </a>

                <a className="text-secondary" href="https://www.linkedin.com/in/john-dominic-fandialan-13a3981a4/" target="_blank" rel="noreferrer noopener">
                    <FontAwesomeIcon icon={['fab', 'linkedin']} className="mx-4 fa-2x" />
                </a>

                <a className="text-secondary" href="https://gitlab.com/jdcfandialan" target="_blank" rel="noreferrer noopener">
                    <FontAwesomeIcon icon={['fab', 'gitlab']} className="mx-4 fa-2x" />
                </a>
            </div>
        </div>
    )
}