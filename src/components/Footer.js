import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export default function Footer(){
    const footerStyle = {
        backgroundColor: 'black',
        height: '25vh'
    }

    const textStyle = {
        color: "whitesmoke"
    }

    return (
        <div style={footerStyle} className="d-flex flex-column align-items-center justify-content-center">
            <h6 className="pt-4" style={textStyle}>dominic.fandialan@gmail.com</h6>
            <h6 style={textStyle}>(+63) 916 730 8554</h6>
            <p className="pt-5 text-center" style={textStyle}>©2020 by John Dominic Fandialan. Artwork by
            <FontAwesomeIcon className="mx-1" icon={['fab', 'instagram']} />
            <a href="https://www.instagram.com/nauthbreigh/" style={{color: '#e94f4f'}} target="_blank" rel="noreferrer noopener">@nauthbreigh</a></p>
        </div>
    )
}