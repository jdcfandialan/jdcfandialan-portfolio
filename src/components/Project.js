import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';

export default function Project(){
    const divStyle = {
        width: '100%',
        height: '100%'
    }

    return (
        <Container fluid style={{backgroundColor: 'white'}}>
            <Row className="py-5">
                <Col lg={{span: 5, offset: 1}} style={{backgroundColor: 'black'}}>
                    <div className="d-flex flex-column justify-content-center" style={divStyle}>
                        <a className="text-secondary px-5 pt-3" style={{textDecoration: 'none'}} href="https://jdcfandialan.gitlab.io/savage/" target="_blank" rel="noreferrer noopener"><h3 style={{color: '#e94f4f', fontWeight: 'bold'}}>SAVAGE</h3></a>
                        <p className="px-5 pt-3" style={{color: "whitesmoke"}}>A static web application that contains information for a fictional Ultimate Frisbee tournament.</p>
                        
                    </div>
                </Col>

                <Col lg={5} >
                    <div className="d-flex align-items-center justify-content-center" style={divStyle}>
                    <a href="https://jdcfandialan.gitlab.io/savage/" target="_blank" rel="noreferrer noopener"><img src={require('../images/savage.png')} alt="" style={{height: '100%', width: '100%', objectFit: 'contain'}} /></a>
                    </div>
                </Col>
            </Row>

            <Row className="py-5">
                <Col lg={{span: 5, offset: 1}} style={{backgroundColor: 'black'}}>
                    <div className="d-flex flex-column justify-content-center" style={divStyle}>
                        <a className="text-secondary px-5 pt-3" style={{textDecoration: 'none'}} href="https://hackathon-group-4.gitlab.io/color-of-the-day/" target="_blank" rel="noreferrer noopener"><h3 style={{color: '#e94f4f', fontWeight: 'bold'}}>Color of the Day</h3></a>
                        <p className="px-5 pt-3" style={{color: "whitesmoke"}}>A quiz-like web application wherein the user answers a series of question and then gets a corresponding color.</p>
                        
                    </div>
                </Col>

                <Col lg={5} >
                    <div className="d-flex align-items-center justify-content-center" style={divStyle}>
                    <a href="https://hackathon-group-4.gitlab.io/color-of-the-day/" target="_blank" rel="noreferrer noopener"><img src={require('../images/cotd.png')} alt="" style={{height: '100%', width: '100%', objectFit: 'contain'}} /></a>
                    </div>
                </Col>
            </Row>

            <Row className="py-5">
                <Col lg={{span: 5, offset: 1}} style={{backgroundColor: 'black'}}>
                    <div className="d-flex flex-column justify-content-center" style={divStyle}>
                        <a className="text-secondary px-5 pt-3" style={{textDecoration: 'none'}} href="https://budgtr.vercel.app/" target="_blank" rel="noreferrer noopener"><h3 style={{color: '#e94f4f', fontWeight: 'bold'}}>Budgtr.</h3></a>
                        <p className="px-5 pt-3" style={{color: "whitesmoke"}}>A budget tracking web application where users can create multiple accounts to track and then create posts for a specific account that will reflect the necessary changes.</p>
                        
                    </div>
                </Col>

                <Col lg={5} >
                    <div className="d-flex align-items-center justify-content-center" style={divStyle}>
                    <a href="https://budgtr.vercel.app/" target="_blank" rel="noreferrer noopener"><img src={require('../images/budgtr.png')} alt="" style={{height: '100%', width: '100%', objectFit: 'contain'}} /></a>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}