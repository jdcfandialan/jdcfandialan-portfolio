export default
[
    {
        id: "001",
        name: "SAVAGE",
        createdUsing: "HTML, CSS",
        description: "A static web application that contains information for a fictional Ultimate Frisbee tournament.",
        link: "https://jdcfandialan.gitlab.io/savage/",
        imgPath: '../images/savage.png'
    },

    {
        id: "002",
        name: "Color of the Day",
        createdUsing: "HTML, CSS, Bootstrap, Javascript",
        description: "A quiz-like web application wherein the user answers a series of question and then gets a corresponding color.",
        link: "https://hackathon-group-4.gitlab.io/color-of-the-day/",
        imgPath: '../images/cotd.png'
    },

    {
        id: "003",
        name: "Budgtr.",
        createdUsing: "MongoDB, ExpressJS, NodeJS, NextJS",
        description: "A budget tracking web application where users can create multiple accounts to track and then create posts for a specific account that will reflect the necessary changes.",
        link: "https://budgtr.vercel.app/",
        imgPath: '../images/budgtr.png'
    }
]
