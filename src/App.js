import React from 'react';
import './App.css';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fab} from '@fortawesome/free-brands-svg-icons';
import Landing from './components/Landing';
import About from './components/About';
import Divider from './components/Divider';
import Footer from './components/Footer';

library.add(fab)

export default function App(){
    return (
        <>
            <Landing />

            <About />

            <Divider />

            <Footer />
        </>
    )
}