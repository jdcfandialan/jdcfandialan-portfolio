import LandingImg from './KD_Final.png';
import BrandingLogo from './KD1.png';
import Savage from './savage.png';
import COTD from './cotd.png';
import Budgtr from './budgtr.png';

export {
    LandingImg,
    BrandingLogo,
    Savage,
    COTD,
    Budgtr
}